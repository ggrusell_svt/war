function niceTime (string) {
    var sec_num = parseInt(string, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    return minutes+':'+seconds;
}


// Initialize your app
var myApp = new Framework7();

// Export selectors engine
var $$ = Dom7;

var sound;

// Add view
var mainView = myApp.addView('.view-main', {
    // Because we use fixed-through navbar we can enable dynamic navbar
    dynamicNavbar: true
});

$$(document).on('pageInit', function (e) {
  initMap();
});



// PLAYER

function playButton(state) {
    if (state == "play") {
        $$('.player-controls .play').addClass("hide");
        $$('.player-controls .pause').removeClass("hide");
    }
    else {
        $$('.player-controls .play').removeClass("hide");
        $$('.player-controls .pause').addClass("hide");
    }
}

function playerFinished() {
    playButton("pause");
}

function loadSound() {
    console.log("loadSound");
    sound = new Howl({
        src: ['https://www.dropbox.com/s/v6in0g0rhgikpog/martin.mp3?dl=1'],
        autoplay: true,
        // volume: 0.5,
        onend: function() {
            playerFinished();
        }
    });
    sound.on('play', function () {
        playButton("play");

        $$('.time .duration').html(niceTime(sound.duration()+''));
        requestAnimationFrame(step.bind(step));
    });
}

function step() {

    var duration = sound.duration();
    var seek = sound.seek() || 0;
    $$('.progress-played').css("width", seek/duration*100+"%");
    $$('.progress-knob').css("margin-left", "calc("+seek/duration*100+"% - 6px)");
    $$('.time .step').html(niceTime((seek)+''));

    if (sound.playing()) {
        requestAnimationFrame(step.bind(step));
    }
}

$$('.player-controls .pause, .player-controls .play').on('click', function () {
    if (sound.state() == "loaded") {
        if (sound.playing()) {
            sound.pause();
            playButton("pause");
        }
        else {
            sound.play();
            playButton("play");
        }
    }
});

$$('.open-picker').on('click', function () {
    loadSound();
});

