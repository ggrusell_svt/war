function ready() {
  console.log("google ready!");
}

var map;

function centerMe() {
       // Try HTML5 geolocation.
       if (navigator.geolocation) {
         navigator.geolocation.getCurrentPosition(function(position) {
           var pos = {
             lat: position.coords.latitude,
             lng: position.coords.longitude
           };

      var myLocation = new google.maps.Marker({
          clickable: false,
          icon: '/dist/img/icon-map-user-location.png',
          shadow: null,
          zIndex: 999,
          map: map
      });

      myLocation.setPosition(pos);

      map.setCenter(pos);
        }, function() {
           handleLocationError(true, infoWindow, map.getCenter());
         });
       } else {
         // Browser doesn't support Geolocation
         handleLocationError(false, infoWindow, map.getCenter());
       }
}

function initMap() {
      console.log("initMap");
       map = new google.maps.Map(document.getElementById('map'), {
         center: {lat: -34.397, lng: 150.644},
         zoom: 15,
         mapTypeControlOptions: { mapTypeIds: [] },
         fullscreenControl: false
       });

centerMe();

 var iconBase = '/dist/img/';
  var icons = {
    active: {
       icon: iconBase + 'icon-map-story-active.png'
    },
    inactive: {
       icon: iconBase + 'icon-map-story-inactive.png'
    }
  };

     function addMarker(feature) {
      var marker = new google.maps.Marker({
        position: feature.position,
        icon: icons[feature.type].icon,
        map: map
      });
      marker.addListener('click', function() {
         openPlayer();
      });
    }

    function openPlayer() {
      myApp.pickerModal('.picker-modal');
      loadSound();
    }

     var featureCollection = {"type":"FeatureCollection","features":[{"type":"Feature","properties":{"image":"https://www.dropbox.com/s/n079c7audo22a0s/img_59.338048_18.102348.jpg?dl=0","author":"ragnar","audio":"https://www.dropbox.com/s/hxb3v8gutn4g341/farfar.mp3?dl=0","tags":[]},"geometry":{"type":"Point","coordinates":[18.102348,59.338048]},"id":"325d989d-cefd-4041-ab35-9408bdd13110"},{"type":"Feature","properties":{"image":"https://www.dropbox.com/s/xtgvuz0q3syxmnv/img_59.336940_18.088747.jpg?dl=0","author":"ragnar","audio":"https://www.dropbox.com/s/v6in0g0rhgikpog/martin.mp3?dl=0","tags":[]},"geometry":{"type":"Point","coordinates":[18.088747,59.33694]},"id":"4a7a353b-dab7-4839-842f-d9f5846ad40e"},{"type":"Feature","properties":{"image":"https://www.dropbox.com/s/n079c7audo22a0s/img_59.338048_18.102348.jpg?dl=0","author":"ragnar","audio":"https://www.dropbox.com/s/35f6oyfegyu8hlk/monedula.mp3?dl=0","tags":[]},"geometry":{"type":"Point","coordinates":[18.102348,59.338048]},"id":"3802bea4-bddb-4393-a9ca-59d5771c3dca"}, {"type":"Feature","properties":{"image":"https://upload.wikimedia.org/wikipedia/commons/4/43/Sveriges_Radio_2008b.jpg (999KB) ","author":"wikipedia","audio":"https://www.dropbox.com/s/44j62712ifzd3m9/radiohuset_22khz_16bit_mono.mp3?dl=1","tags":[]},"geometry":{"type":"Point","coordinates":[18.10138,59.334722]},"id":"73714db3-cdee-4c6e-8fe7-874408b350c7"}]};

    var activeFeatureCollection = {"type":"FeatureCollection","features":[{"type":"Feature","properties":{"image":"https://www.dropbox.com/s/xtgvuz0q3syxmnv/img_59.336940_18.088747.jpg?dl=0","author":"ragnar","audio":"https://www.dropbox.com/s/v6in0g0rhgikpog/martin.mp3?dl=0","tags":[]},"geometry":{"type":"Point","coordinates":[18.088747,59.33694]},"id":"4a7a353b-dab7-4839-842f-d9f5846ad40e"}, {"type":"Feature","properties":{"image":"https://upload.wikimedia.org/wikipedia/commons/4/43/Sveriges_Radio_2008b.jpg (999KB) ","author":"wikipedia","audio":"https://www.dropbox.com/s/44j62712ifzd3m9/radiohuset_22khz_16bit_mono.mp3?dl=1","tags":[]},"geometry":{"type":"Point","coordinates":[18.10138,59.334722]},"id":"73714db3-cdee-4c6e-8fe7-874408b350c7"}]};

     var features = featureCollection.features.map(function(feature) { return {
          position:  new google.maps.LatLng(feature.geometry.coordinates[1], feature.geometry.coordinates[0]),
          type: 'inactive',
          id: feature.id
          };
     });

     var activeFeatures = activeFeatureCollection.features.map(function(feature) { return {
       id: feature.id
     };
   });

   var activeFeatureIds = new Array;

   activeFeatures.forEach(function(activeFeature) {
     activeFeatureIds.push(activeFeature.id);
   });

     features.forEach(function(feature) {
       if(activeFeatureIds.indexOf(feature.id) > -1) {
         feature.type = 'active';
       }
     });

     for (var i = 0, feature; feature = features[i]; i++) {
        addMarker(feature);
      }

     function handleLocationError(browserHasGeolocation, infoWindow, pos) {
       infoWindow.setPosition(pos);
       infoWindow.setContent(browserHasGeolocation ?
                             'Error: The Geolocation service failed.' :
                             'Error: Your browser doesn\'t support geolocation.');
     }

     Dom7('.js-goToLocation').on('click', function () {
         centerMe();
     });
}
